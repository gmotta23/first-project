const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy')
const AuthenticationControllerPolicyLoja = require('./policies/AuthenticationControllerPolicyLoja')

module.exports = (app) => {
  app.post('/cadastro',
    AuthenticationControllerPolicy.cadastro)
  app.post('/loja/:cadastro',
    AuthenticationControllerPolicyLoja.cadastroLoja)
}
