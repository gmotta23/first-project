// ignorar file
module.exports = {
  cadastro (req, res, next) {
    var validation = {
      type: 'object',
      properties: {
        email: { type: 'string', pattern: 'email' },
        password: { type: 'string', pattern: 'password' }
      }
    }
    var result = inspector.validate(validation, req.body)
    if (!result.valid) {
      console.log(result.format())
    }
  }
}

const Joi = require('joi')

module.exports = {
  cadastro (req, res, next) {
    const schema = {
      email: Joi.string().email(),
      password: Joi.string().regex(
        new RegExp('^[a-zA-Z0-9]{8,32}$')
      )
    }

    const { error } = Joi.validate(req.body, schema)

    if (error) {
      switch (error.details[0].context.key) {
        case 'email':
          res.status(400).send({
            error: 'You must provide a valid email address'
          })
          console.log('error')
          break
        case 'password':
          res.status(400).send({
            error: `The password provided failed to match the following rules:
              <br>
              1. It must contain ONLY the following characters: lower case, upper case, numerics.
              <br>
              2. It must be at least 8 characters in length and not greater than 32 characters in length.
            `
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid registration information'
          })
      }
    } else {
      next()
    }
  }
}

module.exports = {
  cadastroLojista (req, res, next) {
    var validation = {
      type: 'object',
      properties: {
        email: { type: 'string', pattern: 'email' },
        password: { type: 'string', minLength: 6 }
      }
    }
    // mexer aqui!
    var result = inspector.validate(validation, req.body)
    if (!result.valid) {
      console.log(result.format())
    }
  }
}

var inspector = require('schema-inspector')

module.exports = {
  cadastro (req, res, next) {
    var validation = {
      type: 'object',
      properties: {
        email: { type: 'string', pattern: 'email' },
        password: { type: 'string', minLength: 6 }
      }
    }
    // mexer aqui!
    const result = inspector.validate(validation, req.body)
    if (!result.valid) {
      // console.log(result.format())
      // console.log(result.error[0].property)
      // console.log(result.error[1])
      // console.log(req.body.constructor)
      // console.log(Object.keys(req.body)[0])
    }

    //   // string dos erros deve ser formada aqui
    var errorCount
    var errorEmail = ''
    var errorPassword = ''
    for (errorCount = 0; errorCount <= Object.keys(req.body).length; errorCount++) {
      console.log(errorCount)
      if (result.error[errorCount] !== undefined) {
        if (result.error[errorCount].property === '@.email') {
          errorEmail = '<br>E-mail inválido'
        }
        if (result.error[errorCount].property === '@.password') {
          errorPassword = '<br>Senha inválida'
        }
      }
    }
    res.status(400).send({
      error: errorEmail + errorPassword
    })
  }
}
