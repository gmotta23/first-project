var inspector = require('schema-inspector')

module.exports = {
  cadastroLoja (req, res, next) {
    var validation = {
      type: 'object',
      properties: {
        email: { type: 'string', pattern: 'email' },
        password: { type: 'string', minLength: 6 }
      }
    }
    // mexer aqui!
    var result = inspector.validate(validation, req.body)
    if (!result.valid) {
      console.log(result.format())
    }
  }
}
