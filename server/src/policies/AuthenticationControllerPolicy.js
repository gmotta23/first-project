const Joi = require('joi')

module.exports = {
  cadastro (req, res, next) {
    const schema = {
      nome: Joi.string().alphanum().min(3).max(30).required(),
      email: Joi.string().email(),
      senha: Joi.string().min(6),
      senhaConfirma: Joi.any().valid(Joi.ref('senha')).required().options({ language: { any: { allowOnly: 'must match senha' } } })

      // regex(
      // new RegExp('^[a-zA-Z0-9]{8,32}$')
      // )
    }

    const { error } = Joi.validate(req.body, schema)
    // console.log(result.error.details[0].context.key)
    // console.log(result.error.details[0].context)
    console.log(error.details[0])
    console.log(error.details[0].context.key)

    if (error) {
      switch (error.details[0].context.key) {
        case 'nome':
          res.status(400).send({
            error: 'Insira um nome válido'
          })
          break
        case 'email':
          res.status(400).send({
            error: 'E-mail inválido'
          })
          break
        case 'senha':
          res.status(400).send({
            error: `A senha deve ter ao menos 6 caracteres`
          })
          break
        case 'senhaConfirma':
          res.status(400).send({
            error: 'As senhas devem ser iguais.'
          })
          break
        default:
          res.status(200).send({
            error: null
          })
      }
    } else {
      next()
    }
  }
}
