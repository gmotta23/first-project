import Vue from 'vue'
import Router from 'vue-router'
import Mainpage from '@/components/Mainpage'
import Cadastro from '@/components/Cadastro'
import Login from '@/components/Login'
import Loja from '@/components/Loja'
import CadastroLoja from '@/components/loja/cadastro'
import LoginLoja from '@/components/loja/login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Mainpage',
      component: Mainpage
    },
    {
      path: '/cadastro',
      name: 'Cadastro',
      component: Cadastro
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/loja',
      name: 'Loja',
      component: Loja
    },
    {
      path: '/loja/cadastro',
      name: 'CadastroLoja',
      component: CadastroLoja
    },
    {
      path: '/loja/login',
      name: 'LoginLoja',
      component: LoginLoja
    }
  ]
})
