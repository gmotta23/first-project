import Api from '@/services/Api'

export default {
  cadastro (dadosCadastro) {
    return Api().post('/cadastro', dadosCadastro)
  },
  cadastroLoja (dadosCadastroLoja) {
    return Api().post('/loja/cadastro', dadosCadastroLoja)
  }
}
